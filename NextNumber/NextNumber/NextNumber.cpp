// NextNumber.cpp : Defines the entry point for the console application.
//
#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

vector<int> got_digits(int number) {
	vector<int> digits;
	while (true){
		int last_curr_digit = number % 10;
		number = number / 10;
		digits.push_back(last_curr_digit);
		if (number == 0)
			break;
	}
	reverse(digits.begin(), digits.end());
	return digits;
}

int find_larger_next_digit(vector<int> digits, int index) {
	for (int i = (int)digits.size() - 1; i > index; i--) {
		if (digits[i] > digits[index]) {
			index = i;
			break;
		}
	}
	return index;
}
int next_number(int number) {
	vector<int> digits;
	digits = got_digits(number);
	if (digits.size() == 1)
		return -1;
	int cur = digits.size() - 1;
	int prev = cur - 1;
	bool in_decsending_order = digits[cur] <= digits[prev];
	while (in_decsending_order && prev!=0) {
		cur--;
		prev--;
		in_decsending_order = digits[cur] <= digits[prev];
	}
	int index_with_digit_for_replace = prev;
	int index_next_larger_digit = find_larger_next_digit(digits, prev);
	swap(digits[index_with_digit_for_replace], digits[index_next_larger_digit]);
	reverse(digits.begin() + index_with_digit_for_replace+1, digits.end());
	int result = 0;
	for (int i = 0; i < digits.size(); i++) {
		result = result * 10 + digits[i];
	}
	if (number>=result)
		return -1;
	return result;
}


int main()
{
	int x;
	cin >> x;
	cout << next_number(x);
	cin >> x;

    return 0;
}

