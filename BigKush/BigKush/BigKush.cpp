// BigKush.cpp : Defines the entry point for the console application.
//

#include <string>
#include <vector>
#include <algorithm>
#include <iostream>
using namespace std;

int main()
{
	long long input_x;
	cin >> input_x;
	int mod10 = input_x % 10;
	if (mod10) {
		cout << mod10;
	}
	else {
		cout << "NO";
	}
	return 0;
}



