// ChessGame.cpp : Defines the entry point for the console application.
//


#include <stdlib.h> 
#include <iostream>
using namespace std;


int main()
{
	int n_size_of_board;
	int m_size_of_board;
	int x1, y1;
	int x2, y2;
	cin >> n_size_of_board;
	cin >> m_size_of_board;
	cin >> x1;
	cin >> y1;
	cin >> x2;
	cin >> y2;
	if (abs(x2 - x1) == abs(y2 - y1))
		cout << "NO";
	else
		cout << "YES";

    return 0;
}

