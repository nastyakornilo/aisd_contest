// MaxNepodpalindrom.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <fstream>
#include <iostream>
#include <string>
using namespace std;

bool all_symboles_equal(string str) {
	int length = str.length();
	for (int i = 0; i < length; i++) {
		if (str[i] != str[0])
			return false;
	}
	return true;
}

bool is_polindrom(string str) {
	int length = str.length();
	for (int i = 0; i < length/2; i++) {
		if (str[i] != str[length - 1 - i])
			return false;
	}
	return true;
}

int max_nepolindrom_length(string str) {
	if (all_symboles_equal(str))
		return -1;
	if (is_polindrom(str))
		return (str.length()-1);
	return str.length();
}

int main(int argc, char* argv[])
{
	string input_str;
	ifstream fin("input.txt");

	fin >> input_str;
	fin.close();

	ofstream fout;
	fout.open("output.txt");
	fout << max_nepolindrom_length(input_str);
	fout.close();

	return 0;
}

