// Inversion.cpp : Defines the entry point for the console application.
//


#include <string>
#include <vector>
#include <algorithm>
#include <iostream>
#include <cctype>
using namespace std;

char change_case(char c) {
	if (std::isupper(c))
		return std::tolower(c);
	else
		return std::toupper(c);
}
string inversion_str(string str,int number_of_q_start [10000], int number_of_q_end[10000]) {
	int curr_inversion_number = number_of_q_start[0];
	if (curr_inversion_number % 2 == 1)
		str[0] = change_case(str[0]);
	int n = str.length() ;
	for (int i = 1; i < n; i++) {
		curr_inversion_number = curr_inversion_number + number_of_q_start[i] - number_of_q_end[i - 1];
		if (curr_inversion_number % 2 == 1)
			str[i] = change_case(str[i]);
	}
	return str;

}
int main()
{
	string input_s;
	int number_of_inquiry;
	cin >> input_s;
	cin >> number_of_inquiry;
	int l;
	int r;
	int start[100000];
	int end[100000];
	for (int i = 0; i < 99999; i++) {
		start[i] = 0;
		end[i] = 0;
	}
	for (int i = 0; i < number_of_inquiry; i++) {
		cin >> l;
		cin >> r;
		if (l > r)
			swap(l, r);
		start[l-1]++;
		end[r-1]++;
	}
	
	cout << inversion_str(input_s,start,end);
	cin >> number_of_inquiry;
    return 0;
}

