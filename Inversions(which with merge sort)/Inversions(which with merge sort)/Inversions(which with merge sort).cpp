// Inversions(which with merge sort).cpp : Defines the entry point for the console application.
//

#include <vector>
#include "iostream"
using namespace std;
vector<int> arr_for_sort;

long merge(int left, int middle, int right)
{
	vector<int> tmp_res_arr;
	for (int i = 0; i < right - left + 1; i++) {
		tmp_res_arr.push_back(0);
	}

	int cur_index = 0;
	int left_pointer = left; 
	int right_pointer = middle; 

	long res = 0;

	while (left_pointer <= middle - 1 && right_pointer <= right) {
		if (arr_for_sort[left_pointer] <= arr_for_sort[right_pointer]) {
			tmp_res_arr[cur_index] = arr_for_sort[left_pointer];
			cur_index++;
			left_pointer++;
		}
		else {
			tmp_res_arr[cur_index] = arr_for_sort[right_pointer];
			res += middle - left_pointer;
			cur_index++;
			right_pointer++;
		}
	}
	
	while (right_pointer <= right) {
		tmp_res_arr[cur_index] = arr_for_sort[right_pointer];
		cur_index++;
		right_pointer++;
	}
	while (left_pointer <= middle - 1) {
		tmp_res_arr[cur_index] = arr_for_sort[left_pointer];
		cur_index++;
		left_pointer++;
	}

	cur_index = 0;
	while (left <= right) {
		arr_for_sort[left] = tmp_res_arr[cur_index];
		left++;
		cur_index++;
	}
	return res;
}

long merge_sort(int left, int right) {
	int middle;
	long res = 0;
	if (right > left) {
		middle = (right + left) / 2;
		res = merge_sort(left, middle);
		res += merge_sort(middle + 1, right);
		res += merge(left, middle + 1, right);
	}
	return res;
}

int main()
{
	int number;
	cin >> number;
	int tmp;
	long res;
	for (int i = 0; i < number; i++)
	{
		cin >> tmp;
		arr_for_sort.push_back(tmp);
		
	}
	res = merge_sort(0, number - 1);
	cout << res;
    return 0;
}

