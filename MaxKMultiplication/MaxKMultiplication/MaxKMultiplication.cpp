// MaxKMultiplication.cpp : Defines the entry point for the console application.
//


#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;


long long find_mod(long long a, long long  mod) {
	long long res = a % mod;
	if (res < 0)
		res += mod;
	return res;
}
int main()
{
	long long MOD = 1000000000 + 7;
	int n;
	int k;
	cin >> n;
	cin >> k;
	std::vector<long long> numbers;
	long long	k_multi = 1;
	//int* numbers = new int[n];
	int tmp = 0;
	for (int i = 0; i < n; i++) {
		cin >> tmp;
		numbers.push_back(tmp);
	}
	sort(numbers.begin(), numbers.end());
	
	if (numbers.back()<=0 && k&1)   //back  k&1 - k -nechet
		for (std::vector<int>::size_type i = n-1; i != n-k-1; i--) {
			numbers[i] = find_mod(numbers[i], MOD);
			k_multi = find_mod(k_multi * numbers[i],MOD);
		}
	else {
		int left = 0;
		int right = numbers.size()-1;
		long long left_multi;
		long long right_multi;
		int counter = 0;
		while (left < right && counter<k) {
			left_multi = numbers[left] * numbers[left + 1];
			right_multi = numbers[right] * numbers[right-1];

			if (left_multi >= right_multi && k>1 && (k-counter!=1) ) {
				numbers[left] = find_mod(numbers[left], MOD);
 				numbers[left+1] = find_mod(numbers[left+1], MOD);
				k_multi = find_mod(k_multi * numbers[left], MOD);
				k_multi = find_mod(k_multi * numbers[left+1], MOD);
				left++;
				left++;
				counter++;
				counter++;
			}
			else {
				numbers[right]= find_mod(numbers[right], MOD);
				k_multi = find_mod(k_multi * numbers[right], MOD);
				right--;
				counter++;
			}
		}
		if (counter != k && left == right) {
			numbers[right] = find_mod(numbers[right], MOD);
			k_multi = find_mod(k_multi * numbers[right], MOD);
			right--;
			counter++;
		}
	}
	cout << k_multi;
	cin >> k;
    return 0;
}

