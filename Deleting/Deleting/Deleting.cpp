// Deleting.cpp : Defines the entry point for the console application.
//


#include <iostream>
#include <vector>

using namespace std;
vector<int> graph_arr[100000];
bool is_used[100000];

void dfs(int verticle) {
	is_used[verticle] = true;
	int neighbor;
	for (int i = 0; i < graph_arr[verticle].size(); i++) {
		neighbor = graph_arr[verticle][i];
		if (!is_used[neighbor])
			dfs(neighbor);
	}
}

int main()
{
	int number_of_vertices;
	int number_of_edges;
	int u;
	int v;

	cin >> number_of_vertices;
	cin >> number_of_edges;

	for (int i = 0; i < number_of_edges; i++) {
		cin >> u;
		cin >> v;
		graph_arr[--u].push_back(--v);
		graph_arr[v].push_back(u);
	}
	int number_of_connectivity_components = 0;
	for (int i = 0; i < number_of_vertices; i++) {
		if (!is_used[i]) {
			dfs(i);
			number_of_connectivity_components++;
		}
	}
	if (number_of_connectivity_components != 1)
		cout<< -1;
	else
    	cout<< number_of_edges-(number_of_vertices - 1);

}