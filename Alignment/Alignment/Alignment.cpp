// Alignment.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stack>
#include <iostream>
#include <vector>
using namespace std;

int main()
{
	long long result = 0;
	int array_size;
	vector<long long>numbers;
	long long tmp;

	cin >> array_size;
	for (int i = 0; i < array_size; i++) {
		cin >> tmp;
		numbers.push_back(tmp);
	}

	stack <long long> st;
	long long last_poped_from_stack = 0;
	long long next_in_numbers;
	int i = 1;
	st.push(numbers[0]);
	while (i < array_size && !st.empty()) {
		last_poped_from_stack = st.top();
		next_in_numbers = numbers[i];
		if (last_poped_from_stack != next_in_numbers) {
			st.pop();
			if (next_in_numbers > last_poped_from_stack) {
				while (!st.empty() && next_in_numbers > st.top()) {
					result += st.top() - last_poped_from_stack;
					last_poped_from_stack = st.top();
					st.pop();
				}
				result += next_in_numbers - last_poped_from_stack;
				st.push(next_in_numbers);
			}

			//}
			else {
				st.push(last_poped_from_stack);
				st.push(next_in_numbers);

			}
		//	i++;
			//}

		}
		i++;
	}

	long long next_last_in_stack = 0;

	if (!st.empty()) {
		last_poped_from_stack = st.top();
		st.pop();
	}
	
	while (!st.empty()) {
			next_last_in_stack = st.top();
			result += next_last_in_stack - last_poped_from_stack;
			st.pop();
			last_poped_from_stack = next_last_in_stack;
	}
	cout << result;
	cin >> result;
    return 0;
}

