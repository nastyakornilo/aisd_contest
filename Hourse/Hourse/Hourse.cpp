// Hourse.cpp : Defines the entry point for the console application.
//

#include <cstdio>
#include <queue>
#include <iostream>
#include <vector>


#pragma comment(lib, "user32")

using namespace std;

bool is_cell_belong_board(int x, int y, int board_x, int board_y) {
	bool x_all_ok = (x>= 0) && (x < board_x);
	bool y_all_ok = (y >= 0) && (y < board_y);
	if (x_all_ok && y_all_ok)
		return true;
	return false;
}


int main() {
	//system("mode 650");
	int board_size_n, board_size_m;
	cin >> board_size_n;
	cin >> board_size_m;
	
	int finish_x, finish_y;
	cin >> finish_x;
	cin >> finish_y;
	finish_x--;
	finish_y--;

	vector<vector<int>> board_cells(board_size_n, vector<int>(board_size_m));
	vector<vector<bool>> is_visited_cell(board_size_n, vector<bool>(board_size_m));

	pair<int, int> hourse_delta_pos[] = { make_pair(2,1), make_pair(2,-1), make_pair(-2,1),make_pair(-2,-1),
		                                  make_pair(1,2),make_pair(1,-2),make_pair(-1,2),make_pair(-1,-2) };	

	queue<pair<int, int> > q;
	q.push(make_pair(0, 0));
	is_visited_cell[0][0] = true;
	int target_x;
	int target_y;
	int x, y;
	while (!q.empty())
	{
		pair<int, int> curr_cell = q.front();
		x = curr_cell.first, y = curr_cell.second;
		
		for (int t = 0; t < 8; t++)
		{
			target_x = x + hourse_delta_pos[t].first;
			target_y = y + hourse_delta_pos[t].second;
			if (is_cell_belong_board(target_x,target_y, board_size_n, board_size_m))
			{
				if (is_visited_cell[target_x][target_y])
					continue;
				q.push(make_pair(target_x, target_y));
				board_cells[target_x][target_y] = board_cells[x][y] + 1;
				is_visited_cell[target_x][target_y] = true;

			}
		}
		q.pop();
		if (x == finish_x && y == finish_y) break;
	
	}


	if (x == finish_x && y == finish_y)
		cout << board_cells[finish_x][finish_y];
	else
		cout << "NEVAR";

}
