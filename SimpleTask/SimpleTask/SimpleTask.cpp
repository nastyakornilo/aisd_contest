// SimpleTask.cpp : Defines the entry point for the console application.
//


#include <string>
#include <vector>
#include <algorithm>
#include <iostream>
using namespace std;

int main()
{
	int len_of_sequence;
	cin >> len_of_sequence;
	std::vector<long long> numbers;
	long long number;
	for (int i = 0; i < len_of_sequence; i++) {
		cin >> number;
		numbers.push_back(number);
	}
	sort(numbers.begin(), numbers.end());
	long long left_multi;
	long long right_multi;
	int left = 0;
	int right = numbers.size() - 1;

	left_multi = numbers[left] * numbers[left + 1];
	right_multi = numbers[right] * numbers[right - 1];
	if (left_multi > right_multi) {
		cout << left_multi;
	}
	else {
		cout << right_multi;
	}
    return 0;
}

