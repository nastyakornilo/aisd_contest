// TheShortestPath.cpp : Defines the entry point for the console application.
//


#include <iostream>
#include <vector>
#include <set>
#include <fstream>


using namespace std;


int main()
{
	int number_of_vertices=0;
	int number_of_edges=0;
	int u;
	int v;
	int weight;

    cin >> number_of_vertices;
	cin >> number_of_edges;
	vector <vector<pair <int, int> > > graph(number_of_vertices);

	for (int i = 0; i < number_of_edges; i++) {
		cin >> u;
		cin >> v;
		cin >> weight;
		
	    graph[--u].push_back(make_pair(--v, weight));
		graph[v].push_back(make_pair(u, weight));
	}

	int start, end;
	cin >> start;
	start--;
	cin >> end;
	end--;

	vector <long long> path(number_of_vertices, 2000000000000000000);
	vector<bool> is_visited(number_of_vertices);

	path[start] = 0;
	set<pair<long long, int>> s;
	
	s.insert(make_pair(path[start],start));

	while (!s.empty())
	{
		int curr_verticle = s.begin()->second;

		s.erase(s.begin());
		for (int i = 0; i < (int)graph[curr_verticle].size(); i++)
		{
			int neigbour_verticle = graph[curr_verticle][i].first;
			int edge_to_negbour_weight = graph[curr_verticle][i].second;

			long long path_to_neigbour_throgh_curr_verticle = path[curr_verticle] + edge_to_negbour_weight;
			if (path[neigbour_verticle] > path_to_neigbour_throgh_curr_verticle) {
				s.erase(make_pair(path[neigbour_verticle], neigbour_verticle));
				path[neigbour_verticle] = path_to_neigbour_throgh_curr_verticle;
				s.insert(make_pair(path[neigbour_verticle], neigbour_verticle));
			}
		}
	}
	cout << path[end];
	cin >> end;

    return 0;
}

